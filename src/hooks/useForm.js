import { useState } from "react";
import ReactForms from "../index";

/**
 * @param {ReactForm.FormOptions} opts
 * @returns {ReactForm.Form}
 */
export default function useForm(opts) {
    let [ form ] = useState(new ReactForms(opts)); //TODO: is this even the right way to do it?
    return form;
};