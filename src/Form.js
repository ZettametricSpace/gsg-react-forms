import React from "react";
import withRef from "./hoc/withRef";
import omit from "lodash/omit";

import FormObject from "./FormObject";

export default impl => withRef(class HookedForm extends React.PureComponent {
    componentDidMount() {
        let { onSubmit, initialValues, initialErrors, onChange, validation } = this.props;
        
        if(onSubmit) impl.onSubmit(onSubmit);
        if(onChange) impl.onChange(onChange);
        if(initialValues) impl.replaceValues(initialValues, true);
        if(initialErrors) impl.replaceErrors(initialErrors, true);
        if(validation) impl.validation = validation;
    }

    componentWillUnmount() {
        let { onSubmit, onChange } = this.props;
        
        if(onSubmit) impl.offSubmit(onSubmit);
        if(onChange) impl.offChange(onChange);
    }

    render() {
        let { children, field, forwardedRef, ...props } = this.props;

        props = omit(props, "onSubmit", "onChange", "initialValues", "initialErrors", "validation");

        let body = children;

        if(field && field.length) {
            body = (
                <FormObject field={field}>{body}</FormObject>
            );
        }

        return (
            <form {...props} ref={forwardedRef} onSubmit={impl.hookSubmit}>
                {body}
            </form>
        );
    }
});
