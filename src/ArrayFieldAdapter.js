import React from "react";
import cloneDeep from "lodash/cloneDeep";
import omit from "lodash/omit";
// import { determineValueGetter } from "./utils";
// import isEqual from "lodash/isEqual";

export default class ArrayFieldAdapter extends React.PureComponent {
    onChange = index => e => {
        let { value = []/*, field, valueGetter*/, onChange } = this.props;

        let val = cloneDeep(value);
        // val[index] = valueGetter(e, field);
        val[index] = e;

        onChange(val, e);
    }

    renderValue = (value, props) => {
        let { children, keyProp, fieldComponent: Component, fieldProps } = this.props;

        let body = Component ? (
            <Component {...omit(fieldProps, "keyProp")} {...props} />
        ) : (
            children(value, props)
        );

        return (
            <React.Fragment key={value[keyProp]}>
                {body}
            </React.Fragment>
        );
    }

    render() {
        let { children, value = [], keyProp, valueProp, onChangeProp, fieldComponent } = this.props;

        if(typeof children !== "function" && fieldComponent === undefined)
            throw new Error("Must pass either component property or rendering function in children to ArrayFieldAdapter!");
        if(!keyProp)
            throw new Error("Must pass keyProp to ArrayFieldAdapter!");

        return value.map((x, i) => {
            let props = {
                [valueProp]: x,
                [onChangeProp]: this.onChange(i)
            };

            return this.renderValue(x, props);
        });
    }
}