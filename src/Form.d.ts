import * as React from "react";
import { ReactFormValidation } from "./common";

interface FormProps {
    onSubmit?: Function;
    onChange?: Function;

    initialValues?: {};
    initialErrors?: {};

    validation?: ReactFormValidation;
}

interface Form extends React.PureComponent<FormProps> {}
export = Form;