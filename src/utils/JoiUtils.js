const getPart = (schema, prop) => {
    if(!schema.schema) return null;

    let parts = prop.split(".");

    let root = schema;
    for(let part of parts) {
        let segment = root._ids._byKey.get(part);
        if(!segment) return null;

        root = segment;
    }

    return root;
};

/**
 * Validate a single property of an object schema
 * @param {import("@hapi/joi").ObjectSchema} schema 
 * @param {string} prop 
 * @param {any} value 
 * @returns {import("@hapi/joi").ValidationResult}
 */
export const validatePartial = (schema, prop, value) => {
    let part = getPart(schema, prop);
    if(!part) return { error: null, value }; //If not existent in the schema, should we ignore it?

    part = part.schema;
    
    let messages = Object.keys(schema._preferences.messages).reduce((msgs, key) => ({
        ...msgs,
        [key]: schema._preferences.messages[key].source
    }), {});

    return part.messages(messages).validate(value);
};

export default {
    validatePartial
};
