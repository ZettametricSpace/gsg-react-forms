import * as React from "react";
import { ReactFormValidation } from "./common";
import { EventEmitter } from "events";

import Form from "./Form";

interface ReactFormOpts {
    name?: string;

    onSubmit?: Function;
    onChange?: Function;
    onError?: Function;
    onUpdate?: Function;

    debugUpdates?: boolean;
    debugChanges?: boolean;

    validateOnBlur?: boolean;
    validation?: ReactFormValidation;

    initialValues: {};
    initialErrors: {};
}

interface HookFieldArgs {
    name: string;
    defaultValue?: string | any;
    validateOnBlur?: boolean;
    value: any;
    valueGetter?: string | Function;
    valueProp?: string;
    onChange?: Function;
    onBlur?: Function;
    props: {};
}

declare class ReactForm extends EventEmitter {
    name: string;
    validateOnBlur: boolean;
    validation: ReactFormValidation;

    Form: Form;
    Fields: { [field: string]: React.Component; }

    constructor(opts: ReactFormOpts);

    onChange(...listeners: Function[]): void;
    offChange(...listeners: Function[]): void;
    
    onSubmit(...listeners: Function[]): void;
    offSubmit(...listeners: Function[]): void;
    
    hookField(options: HookFieldArgs): {};

    getState(): { errors: {}, values: {} };

    values(): {};
    values(...fields: string[]): Array<any>;
    values(values: {}): void;

    errors(): {};
    errors(...fields: string[]): Array<any>;
    errors(errors: {}): void;

    value(field: string): any;
    value(field: string, value: any): void;

    error(field: string): any;
    error(field: string, value: any): void;

    replaceValues(values: {}, initial?: boolean): void;
    replaceErrors(errors: {}, initial?: boolean): void;

    resetValues(): void;
    resetErrors(): void;
    resetForm(): void;

    validate(field: string, next?: (error: any, field: string) => void): Promise<void>;

    change(field: string, callback?: (field: string) => void): void;
    update(field: string): boolean;

    hookSubmit(e: Event): Promise<void>;
}

export = ReactForm;