module.exports = {
    type: "react-component",
    babel: {
        presets: [ "@babel/preset-env", "@babel/preset-react" ],
        plugins: [
            ["@babel/plugin-proposal-class-properties", { loose: true }],
            ["module-resolver", {
                root: ["./src"]
            }]
        ]
    },
    npm: {
        esModules: true,
        umd: false
    }
}
