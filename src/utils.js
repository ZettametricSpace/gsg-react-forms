import ArrayFieldAdapter from "./ArrayFieldAdapter";
import omit from "lodash/omit";
import get from "lodash/get";
import joi from "@hapi/joi";

export const mergeProperties = (...props) =>
    props.filter(x => x && x.length).join(".");

export const getHookTemplate = (field, context = {}, fieldProps) => {
    let { objectProp } = context;
    let {
        component: Component,
        valueGetter = "target.value",
        valueProp = "value",
        errorProp = "error",
        onChangeProp = "onChange",
        onBlurProp = "onBlur",
        onChange, onBlur,
        defaultValue,
        componentProps = {},
        template,
        forwardedRef,
        validateOnBlur,
        ...props
    } = fieldProps;

    let _template = {
        name: field,
        defaultValue,
        valueGetter: determineValueGetter(valueGetter),
        valueProp, errorProp, onChangeProp, onBlurProp,
        onChange, onBlur,
        validateOnBlur,
        target: mergeProperties(objectProp, field),
        props: componentProps
    };

    let res = { Component, ref: forwardedRef, props: omit(props, "name"), template: _template };

    switch(template) {
        case "checkbox": {
            _template.valueGetter = "target.checked";
            _template.valueProp = "checked";
            _template.props.type = "checkbox";
        } break;

        case "select": {
            _template.valueGetter = v => v;
        } break;

        case "array": {
            res.Component = ArrayFieldAdapter;

            _template.props.field = field;
            _template.props.fieldComponent = Component;
            _template.props.fieldProps = componentProps;
            _template.props.valueGetter = _template.valueGetter;
            _template.props.valueProp = _template.valueProp;
            _template.props.onChangeProp = _template.onChangeProp;

            _template.defaultValue = [];
            _template.valueProp = "value";
            _template.onChangeProp = "onChange";
            _template.valueGetter = v => v;
        } break;
    }

    return res;
};

export const determineValueGetter = (valueGetter) => {
    if(typeof valueGetter === "function") {
        return valueGetter;
    } else if(typeof valueGetter === "string") {
        return e => get(e, valueGetter);
    } else {
        return e => get(e, "target.value", "");
    }
};

export const validateForm = async(form, values) => {
    let validation = form.validation;

    if(joi.isSchema(validation)) {
        let { error, value } = validation.validate(values, { allowUnknown: true, abortEarly: false });

        if(error) {
            return {
                error: error.details.reduce((obj, err) => ({
                    ...obj,
                    [err.context.key]: err.message
                }), {}),
                value
            };
        } else {
            return { error: null, value };
        }
    }

    if(typeof validation === "function") {
        let errors = {};
        let promise = validation(values, errors);

        if(promise && promise.then) await promise;

        return { error: errors, value: values };
    }

    if(!validation)
        return { error: null, value: values };

    throw new Error("Invalid type for validation. Must be function or joi schema");
};
