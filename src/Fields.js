import React from "react";
import withRef from "./hoc/withRef";
import { getHookTemplate, mergeProperties } from "./utils";
import MetadataContext from "./MetadataContext";

const IGNORED_PROPS = [
    "_cachedFields", "form"
];

class FormFields {
    _cachedFields = {};

    constructor(form) {
        this.form = form;
    }

    // FormFields, "company"
    get(self, prop) {
        if(IGNORED_PROPS.includes(prop)) return self[prop];

        if(this._cachedFields[prop])
            return this._cachedFields[prop];

        let form = this.form;

        this._cachedFields[prop] = withRef(class FormWrappedFieldComponent extends React.PureComponent {
            static contextType = MetadataContext;
            
            onUpdate = () => this.forceUpdate();
            
            componentDidMount() {
                let { objectProp } = this.context || {};
                let key = mergeProperties(objectProp, prop);

                form.onChange(key, this.onUpdate);
            }

            componentWillUnmount() {
                let { objectProp } = this.context || {};
                let key = mergeProperties(objectProp, prop);

                form.offChange(key, this.onUpdate);
            }

            render() {
                let { Component, ref, template, props } = getHookTemplate(prop, this.context, this.props);
                let hook = form.hookField(template);

                if(Component.name === "ArrayFieldAdapter") {
                    return (
                        <Component {...hook} {...props} template={template} hook={hook} ref={ref} />
                    );
                } else {
                    return (
                        <Component {...hook} {...props} ref={ref} />
                    );
                }

            }
        });

        return this._cachedFields[prop];
    }
}

/**
 * @returns {ReactForm.FormFields}
 */
export default impl => {
    let fields = new FormFields(impl);
    return new Proxy(fields, fields);
};
