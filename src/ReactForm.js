import JoiUtils from "./utils/JoiUtils";
import joi from "@hapi/joi";
import { EventEmitter } from "events";
import { determineValueGetter, validateForm } from "./utils";
import get from "lodash/get";
import set from "lodash/set";
import unset from "lodash/unset";
import uniq from "lodash/uniq";

import setupForm from "./Form";
import setupFields from "./Fields";
import FormObject from "./FormObject";

// Because _.get will return null or undefined, we can't do standard truthy/falsy or === undefined operations. This lets us conveniently check if something genuinely isn't defined because Symbol() generates a unique symbol every time
const UNDEFINED = Symbol("undefined");

export default class ReactForm extends EventEmitter {
    #errors = {};
    #values = {};
    #initialValues = {};
    #initialErrors = {};

    name = "form";
    validateOnBlur = true;
    validation = joi.object();

    constructor({
        name = "form",

        onSubmit,
        onChange,
        onError,
        onUpdate,

        debugUpdates = false,
        debugChanges = false,

        validateOnBlur = true,
        validation = joi.object(),

        initialValues = {},
        initialErrors = {}
    } = {}) {
        super();

        this.name = name;

        this.#initialValues = initialValues || {};
        this.#initialErrors = initialErrors || {};
        this.#values = initialValues || {};
        this.validateOnBlur = validateOnBlur;
        this.validation = validation;

        if(onSubmit) this.on("submit", onSubmit);
        if(onChange) this.on("change", onChange);
        if(onError) this.on("errors", onError);
        if(onUpdate) this.on("update", onUpdate);

        this.Form = setupForm(this);
        this.Fields = setupFields(this);
        this.FormObject = FormObject;

        if(debugUpdates) this.on("update", console.log.bind(console, `${name}-update`));
        if(debugChanges) this.on("change", console.log.bind(console, `${name}-change`));
    }

    /**
     * @param {...Function} listeners
     */
    onChange = (...listeners) => {
        if(typeof listeners[0] === "string" && listeners.length > 1) {
            // On change specific field
            this.on(`change-${listeners[0]}`, ...listeners.slice(1));
        } else {
            // Generalized on change
            this.on("change", ...listeners);
        }
    }

    /**
     * @param {...Function} listeners
     */
    offChange = (...listeners) => {
        if(typeof listeners[0] === "string" && listeners.length > 1) {
            // On change specific field
            this.off(`change-${listeners[0]}`, ...listeners.slice(1));
        } else {
            // Generalized on change
            this.off("change", ...listeners);
        }
    };

    /**
     * @param {...Function} listeners
     */
    onSubmit = (...listeners) => {
        this.on("submit", ...listeners);
    };

    /**
     * @param {...Function} listeners
     */
    offSubmit = (...listeners) => {
        this.off("submit", ...listeners);
    };

    hookField = ({
        name,
        defaultValue = "",
        validateOnBlur,
        value,
        valueGetter = "target.value",
        valueProp = "value",
        errorProp = "error",
        onChangeProp = "onChange",
        onBlurProp = "onBlur",
        onChange,
        onBlur,
        target,
        props = {}
    }) => {
        if(get(this.#values, target, UNDEFINED) === UNDEFINED)
            set(this.#values, target, defaultValue);

        if(value !== undefined)
            set(this.#values, target, value);

        return {
            [valueProp]: get(this.#values, target),
            id: target,
            name: target,
            [errorProp]: get(this.#errors, target),
            [onChangeProp]: this.#onChange({
                field: target,
                valueGetter: determineValueGetter(valueGetter),
                handler: onChange
            }),
            [onBlurProp]: this.#onBlur({
                field: target,
                validateOnBlur: validateOnBlur,
                handler: onBlur
            }),
            ...props
        };
    };

    getState = () => ({
        errors: this.#errors,
        values: this.#values
    });

    values = (...fields) => {
        if(fields.length === 0) {
            return {...this.#values};
        } else if(fields.length === 1 && typeof fields[0] === "object") {
            Object.keys(fields[0]).map(field => this.value(field, fields[0][field]));
        } else return fields.map(x => get(this.#values, x));
    };

    errors = (...fields) => {
        if(fields.length === 0) {
            return {...this.#errors};
        } else if(fields.length === 1 && typeof fields[0] === "object") {
            Object.keys(fields[0]).map(field => this.error(field, fields[0][field]));
        } else return fields.map(x => get(this.#errors, x));
    };

    value = (field, value) => {
        if(value !== undefined) {
            set(this.#values, field, value);
            this.validate(field, error => {
                if(!error) this.change(field);
            });
        } else return get(this.#values, field);
    }

    error = (field, error) => {
        if(error !== undefined) {
            let _err = get(this.#errors, field, UNDEFINED);

            if(error && _err === UNDEFINED) {
                set(this.#errors, field, error);
                this.change(field);
            } else if(!error && _err !== UNDEFINED) {
                unset(this.#errors, field);
                this.change(field);
            }
        } else return get(this.#errors, field);
    }

    // These two should be used only in "./Form" to replace initial values/errors on mount
    replaceValues = (obj, initial = false) => {
        this.#values = obj;

        if(initial) {
            this.#initialValues = obj;
        }
    }
    
    replaceErrors = (obj, initial = false) => {
        this.#errors = obj;

        if(initial) {
            this.#initialErrors = obj;
        }
    }

    resetValues = () => this.values({...this.#initialValues});
    resetErrors = () => this.errors({...this.#initialErrors});
    resetForm = () => {
        let fields = Object.keys(this.#values);

        this.#values = {...this.#initialValues};
        this.#errors = {...this.#initialErrors};

        fields.map(field => this.change(field));
    };

    setState = (values = this.#values, errors = this.#errors) => {
        let fields = Object.keys(this.#values);

        this.#values = values;
        this.#errors = errors;

        fields = uniq([...fields, ...Object.keys(this.#values)]);
        fields.map(field => this.change(field));
    }

    /**
     * @param {string} field
     */
    validate = async(field, next = () => null) => {
        if(!field) {
            let { error, value } = await validateForm(this, this.#values);
            this.#errors = {};
    
            if(error) {
                this.#errors = error;
            }
    
            this.emit("errors", this.#errors, this);
            this.emit("update", { errors: this.#errors, values: this.#values }, this);
    
            Object.keys(this.#values).forEach(field => this.update(field));
    
            if(!error) {
                this.emit("submit", value, this);
            }
        } else {
            if(!this.validateOnBlur) return next(null, field);
    
            if(joi.isSchema(this.validation)) {
                let { error } = JoiUtils.validatePartial(this.validation, field, get(this.#values, field));
                error = error ? error.message : undefined;
        
                this.error(field, error);
                return next(error, field);
            } else {
                throw new Error("Partial validation not available for function validators. Please mark the form as `validateOnBlur={false}` or { validateOnBlur: false } on init");
            }
        }
    }

    /**
     * @param {string} field
     */
    change = (field, callback) => {
        this.emit("change", { field: field, value: get(this.#values, field), error: get(this.#errors, field) }, this);
        this.emit("update", { errors: this.#errors, values: this.#values }, this);
        this.update(field);

        if(callback) callback(field);
    };

    /**
     * @param {string} field
     */
    update = field =>
        this.emit(`change-${field}`, { value: get(this.#values, field), error: get(this.#errors, field) }, this);

    #onChange = ({ field, valueGetter, handler }) => e => {
        if(get(this.#errors, field))
            unset(this.#errors, field);

        set(this.#values, field, valueGetter(e, field));
        this.change(field, handler);
    }

    #onBlur = (opts = {}) => e => {
        let { field, validateOnBlur, handler = () => null } = opts;
        let localOverride = opts.hasOwnProperty("validateOnBlur") && opts.validateOnBlur !== undefined;

        if(localOverride ? validateOnBlur : this.validateOnBlur) {
            this.validate(field, () => handler(e));
        } else return handler(e);
    }

    hookSubmit = async(e) => {
        if(e && e.preventDefault) e.preventDefault();
        return await this.validate();
    };
}
